<?php
/*
Template Name: Services
*/

$contents = carbon_get_post_meta( get_the_ID(), 'crb_contents' );
?>

<?php get_header(); ?>
	<main class="services-page page" id="services-page">
		<section class="first-screen tabs-head-page" id="first-screen">
			<div class="container main-slider-wrapper">
				<div class="image-wrapper " id="main-image-slider">
					<?php
					foreach ( $contents as $item ) { ?>
						<div class="photo-category ">
							<div class="mainscreen-tab">
								<div class="tab-slide">
									<?php echo wp_kses_post( wp_get_attachment_image( $item['photo'], 'full' ) ); ?>
								</div>
							</div>
						</div>

					<?php } ?>
				</div>
				<div class="text-section">
					<h1><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_banner_title' ) ); ?></h1>
				</div>
				<div class="categories-section">
					<?php
					foreach ( $contents as $item ) { ?>
						<div class="category">
							<span><?php echo wp_kses_post( $item['category_name'] ) ?></span>
						</div>
					<?php } ?>
				</div>
				<div class="breadcrumbs">
					<a href="<?php echo esc_url( get_home_url() ); ?>">
						<?php echo esc_html( carbon_get_the_post_meta( 'crb_crumbs_1' ) ); ?>
					</a> / <span><?php echo esc_html( carbon_get_the_post_meta( 'crb_crumbs_2' ) ); ?></span>
				</div>
			</div>
		</section>

		<!-- TABS_SECTION -->
		<section class="second-screen tabs-container light-section">
			<div class="container">
				<div class="wrap-content">
					<?php
					foreach ( $contents as $item ) { ?>
						<div class="tab-content-wrapper">
							<div class="image">
								<?php echo wp_kses_post( wp_get_attachment_image( $item['photo_2'], 'full' ) ); ?>
							</div>
							<div class="desc">
								<?php echo wp_kses_post( apply_filters( 'the_content', $item['text'] ) ); ?>
							</div>
						</div>
					<?php } ?>

				</div>
			</div>
		</section>

		<!-- FURNITURE_SLIDER -->
		<section class="third-screen tabs-content light-section">
			<div class="container title-wrapper">
				<h3>Наша фурнитура</h3>
				<p>Конечный потребитель выбирает кухонный комплект, в основном ориентируясь на его внешний вид. А вот
					мастер-мебельщик знает, что цвет и материал изготовления фасадов, форма дверок и количество полок –
					это далеко не все характеристики гарнитура. Не менее важной деталью является фурнитура, от
					надёжности которой зависит долговечность и полноценность функционирования шкафов и тумбочек.</p>
			</div>
			<?php get_template_part( 'template-parts/content', 'slider-gallery1' ); ?>
		</section>

		<!--  GLOBAL CONTENT CONTACT_FORM-->
		<section class="fourth-screen contact-form-section" id="contact-form">
			<?php get_template_part( 'template-parts/content', 'contact-form' ); ?>
		</section>

		<!--  GLOBAL CONTENT INSTARGAM_POSTS_SECTION-->
		<section class="fifth-screen insagram-section light-section" id="instagram-section">
			<?php get_template_part( 'template-parts/content', 'instagram-section' ); ?>
		</section>

	</main>
<?php
get_footer();
