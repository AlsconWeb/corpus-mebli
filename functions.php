<?php
/**
 * korpus-mebli functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package korpus-mebli
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'korpus_mebli_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function korpus_mebli_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on korpus-mebli, use a find and replace
		 * to change 'korpus-mebli' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'korpus-mebli', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'nav-menu' => esc_html__( 'Nav-menu', 'korpus-mebli' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'korpus_mebli_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);
	}
endif;
add_action( 'after_setup_theme', 'korpus_mebli_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function korpus_mebli_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'korpus_mebli_content_width', 640 );
}

add_action( 'after_setup_theme', 'korpus_mebli_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function korpus_mebli_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'korpus-mebli' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'korpus-mebli' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'korpus_mebli_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function korpus_mebli_scripts() {
	wp_enqueue_style( 'korpus-mebeli-style', get_stylesheet_uri(), [], _S_VERSION );

	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/assets/slick/slick.css', [], _S_VERSION );
	wp_enqueue_style( 'slick-theme-style', get_template_directory_uri() . '/assets/slick/slick-theme.css', [], _S_VERSION );
	wp_enqueue_style( 'modal-style', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css', [], _S_VERSION );

	wp_enqueue_style( 'korpus-mebeli-style-global', get_template_directory_uri() . '/assets/styles/global.css', [], null );
	wp_enqueue_style( 'korpus-mebeli-style-header', get_template_directory_uri() . '/assets/styles/header.css', [], null );
	wp_enqueue_style( 'korpus-mebeli-style-footer', get_template_directory_uri() . '/assets/styles/footer.css', [], null );

	if ( is_page_template( 'templates/template-home-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-home', get_template_directory_uri() . '/assets/styles/page-home.css', [], null );
	}
	if ( is_page_template( 'templates/template-portfolio-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-services', get_template_directory_uri() . '/assets/styles/page-portfolio.css', [], null );
	}
	if ( is_page_template( 'templates/template-services-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-services', get_template_directory_uri() . '/assets/styles/page-services.css', [], null );
	}
	if ( is_page_template( 'templates/template-project-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-project', get_template_directory_uri() . '/assets/styles/project.css', [], null );
	}
	if ( is_page_template( 'templates/template-feedbacks-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-feedback', get_template_directory_uri() . '/assets/styles/page-feedbacks.css', [], null );
	}
	if ( is_page_template( 'templates/template-our-partners.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-feedback', get_template_directory_uri() . '/assets/styles/page-partners.css', [], null );
	}
	if ( is_page_template( 'templates/template-contacts-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-contacts', get_template_directory_uri() . '/assets/styles/page-contacts.css', [], null );
	}
	if ( is_page_template( 'templates/template-404-page.php' ) ) {
		wp_enqueue_style( 'korpus-mebeli-style-404', get_template_directory_uri() . '/assets/styles/page-404.css', [], null );
	}

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/slick/slick.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'inputmask', get_template_directory_uri() . '/assets/inputmask/jquery.inputmask.js', [ 'jquery' ], null, true );
	wp_enqueue_script( 'modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', [ 'jquery' ], null, true );
	wp_enqueue_script( 'korpus-mebli-main-js', get_template_directory_uri() . '/assets/js/main.js', [ 'jquery' ], _S_VERSION, true );

}

add_action( 'wp_enqueue_scripts', 'korpus_mebli_scripts' );


/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// CARBON_FIELDS
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	Container::make( 'theme_options', __( 'Theme Options' ) )
		->add_tab( __( 'Contacts' ), [
			Field::make( 'text', 'inst_link', __( 'Link instagram' ) )
				->set_default_value( '#' )
				->set_width( 50 ),
			Field::make( 'text', 'text_link', __( 'Text instagram' ) )
				->set_default_value( '@korpusmebeli' )
				->set_width( 50 ),
			Field::make( 'text', 'phone', __( 'Phone' ) )
				->set_default_value( '+38 093 561 96 49' )
				->set_width( 50 ),
			Field::make( 'text', 'email', __( 'Email' ) )
				->set_default_value( 'korpusmebeli@gmail.com' )
				->set_width( 50 ),
			Field::make( 'text', 'adr', __( 'Location' ) )
				->set_default_value( 'Харьков, ул. Динамическая 2а' )
				->set_width( 50 ),
			Field::make( 'text', 'adr_link', __( 'Location Link' ) )
				->set_default_value( 'https://goo.gl/maps/dxL9LsXQEQXjaQYL7' )
				->set_width( 50 ),
			Field::make( 'text', 'time', __( 'Work time' ) )
				->set_default_value( 'пн-пт, 10:00 – 19:00' )
				->set_width( 50 ),
			Field::make( 'textarea', 'additional', __( 'Additional' ) )
				->set_default_value( '<p>© 2021 Korpus mebeli</p>
                <p>Сделано в <a href="https://www.art-cube.com.ua/">ART CUBE Digital Agency</a></p>' )
				->set_width( 100 ),
		] )
		->add_tab( __( 'Instagram gallery' ), [

			Field::make( 'complex', 'crb_instagram', __( 'Instagram' ) )
				->add_fields( [
					Field::make( 'text', 'link', __( 'Link' ) ),
					Field::make( 'image', 'photo', __( 'Photo' ) ),
				] ),
		] );
}

add_action( 'carbon_fields_register_fields', 'crb_attach_pages_options' );
function crb_attach_pages_options() {
	Container::make( 'post_meta', __( 'Home Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-home-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Корпусная мебель на заказ' ),
			Field::make( 'text', 'crb_banner_sub_title', __( 'Sub title' ) )
				->set_default_value( 'Партнер по изготовлению мебели, который
                    специализируется на домашних интерьерах по высшим стандартам.' ),
			Field::make( 'text', 'crb_banner_btn', __( 'Button text' ) )
				->set_default_value( 'Заказать консультацию' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'complex', 'crb_categories', __( 'Content' ) )
				->add_fields( [
					Field::make( 'text', 'cat', __( 'Category name' ) )
						->set_default_value( 'Cat name' ),
					Field::make( 'complex', 'photos', __( 'Photos' ) )
						->add_fields( [
							Field::make( 'image', 'photo', __( 'Category Photo' ) ),
						] ),
				] ),
		] )
		->add_tab( __( 'Services section' ), [
			Field::make( 'text', 'crb_services_title', __( 'Title Text' ) )
				->set_default_value( 'Услуги' ),
			Field::make( 'text', 'crb_services_sub_title', __( 'Sub-Title Text' ) )
				->set_default_value( 'Замеряем. Создаем. Производим. Монтируем' ),
			Field::make( 'text', 'crb_services_btn', __( 'Btn Text' ) )
				->set_default_value( 'Узнать подробнее' ),
			Field::make( 'text', 'crb_services_link', __( 'Btn Link' ) )
				->set_default_value( '#' ),
			Field::make( 'complex', 'crb_services', __( 'Services list' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Photo' ) ),
					Field::make( 'text', 'btn', __( 'Btn Text' ) )
						->set_default_value( 'Жилая площадь<span>01</span>' ),
					Field::make( 'text', 'btn_link', __( 'Btn Link' ) )
						->set_default_value( '#' ),
				] ),
		] )
		->add_tab( __( 'Benefits section' ), [
			Field::make( 'text', 'crb_benefits_title', __( 'Title Text' ) )
				->set_default_value( 'Премущества работы с нами' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_1', __( 'Text 1' ) )
				->set_default_value( 'Индивидуальный подход' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_2', __( 'Text 2' ) )
				->set_default_value( 'Бесплатный выезд замерщика по Харькову и области' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_3', __( 'Text 3' ) )
				->set_default_value( 'Бесплатная консультация' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_4', __( 'Text 4' ) )
				->set_default_value( 'Создание дизайна на этапе строительства дома' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_5', __( 'Text 5' ) )
				->set_default_value( 'Работа официально по договору' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_6', __( 'Text 6' ) )
				->set_default_value( 'Опыт работы более 25 лет' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_7', __( 'Text 7' ) )
				->set_default_value( 'Работаем напрямую с поставщиками, цены - ниже' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_benefits_8', __( 'Text 8' ) )
				->set_default_value( 'Работаем только с сертифицированными материалами' )
				->set_width( 50 ),
		] )
		->add_tab( __( 'Slider section' ), [
			Field::make( 'text', 'crb_slider_title', __( 'Title Text' ) )
				->set_default_value( 'Примеры наших работ' ),
			Field::make( 'complex', 'crb_slider', __( 'Services list' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Photo' ) ),
					Field::make( 'text', 'btn', __( 'Btn Text' ) )
						->set_default_value( 'Жилая площадь<span>01</span>' ),
					Field::make( 'text', 'btn_link', __( 'Btn Link' ) )
						->set_default_value( '#' ),
				] ),
		] )
		->add_tab( __( 'Steps section' ), [
			Field::make( 'rich_text', 'crb_steps_content', __( 'Content' ) )
				->set_default_value( '<h3>Путь от идеи до готового продукта</h3>
                        <h4>Что входит в пакет наших услуг</h4>
                        <ul>
                            Дизайн проекта
                            <li>Предварительный замер</li>
                            <li>Широкий выбор материалов</li>
                            <li>Создание дизайна мебели</li>
                            <li>Предварительный замер</li>
                            <li>Контрольный замер</li>
                            <li>Согласование проекта с заказчиком</li>
                        </ul>' ),
			Field::make( 'complex', 'crb_steps_slider', __( 'Services list' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Photo' ) ),
				] ),

		] )
		->add_tab( __( 'Partners section' ), [
			Field::make( 'text', 'crb_partners_title', __( 'Title text' ) )
				->set_default_value( 'Наши партнеры' ),
			Field::make( 'complex', 'crb_partners_slider', __( 'Partners list' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Photo' ) )
						->set_value_type( 'url' ),
				] ),
			Field::make( 'text', 'btn_steps_link', __( 'Btn Link' ) )
				->set_default_value( '#' ),
		] );

	Container::make( 'post_meta', __( 'Single project Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-project-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Технологичная <br>детская кроватка' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Text services' ) )
				->set_default_value( 'Услуги' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_crumbs_3', __( 'Link services' ) )
				->set_default_value( 'https://corpus-mebli.com.ua/services/' )
				->set_width( 25 ),
			Field::make( 'text', 'crb_crumbs_4', __( 'Txt current' ) )
				->set_default_value( 'Технологичная детская кроватка' )
				->set_width( 25 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'image', 'crb_banner_image', __( 'Image' ) )
				->set_value_type( 'url' ),
		] )
		->add_tab( __( 'Content section' ), [
			Field::make( 'complex', 'crb_content', __( 'Content' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Content Photo' ) ),
					Field::make( 'rich_text', 'text', __( 'Content Text' ) ),
				] ),
		] )
		->add_tab( __( 'Slider section' ), [
			Field::make( 'rich_text', 'crb_slider_text', __( 'Slider Text' ) )
				->set_default_value( '<h3>Наша фурнитура</h3>
                <p>Конечный потребитель выбирает кухонный комплект, в основном ориентируясь на его внешний вид. А вот мастер-мебельщик знает, что цвет и материал изготовления фасадов, форма дверок и количество полок – это далеко не все характеристики гарнитура. Не менее важной деталью является фурнитура, от надёжности которой зависит долговечность и полноценность функционирования шкафов и тумбочек.</p>' ),
			Field::make( 'complex', 'crb_slider', __( 'Content' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Content Photo' ) ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Services Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-services-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Услгуги' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Услгуги' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'complex', 'crb_content', __( 'Content' ) )
				->add_fields( [
					Field::make( 'text', 'category_name_service', __( 'Category name' ) )
						->set_default_value( 'Cat name' ),
					Field::make( 'image', 'photo', __( 'Banner Photo' ) ),
					Field::make( 'rich_text', 'text', __( 'Content Text' ) ),
					Field::make( 'image', 'photo_2', __( 'Content Photo' ) ),
				] ),
		] )
		->add_tab( __( 'Slider section' ), [
			Field::make( 'rich_text', 'crb_slider_text', __( 'Slider Text' ) )
				->set_default_value( '<h3>Наша фурнитура</h3>
                <p>Конечный потребитель выбирает кухонный комплект, в основном ориентируясь на его внешний вид. А вот мастер-мебельщик знает, что цвет и материал изготовления фасадов, форма дверок и количество полок – это далеко не все характеристики гарнитура. Не менее важной деталью является фурнитура, от надёжности которой зависит долговечность и полноценность функционирования шкафов и тумбочек.</p>' ),
			Field::make( 'complex', 'crb_slider', __( 'Content' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Content Photo' ) ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Partners Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-our-partners.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Наши партнеры' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Наши партнеры' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'image', 'crb_banner_image', __( 'Image' ) )
				->set_value_type( 'url' ),
		] )
		->add_tab( __( 'Content section' ), [
			Field::make( 'image', 'crb_about_photo', __( 'Content Photo' ) ),
			Field::make( 'rich_text', 'crb_about_text', __( 'Content Text' ) ),
			Field::make( 'complex', 'crb_partners', __( 'Partners' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Partner Photo' ) ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Feedbacks Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-feedbacks-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Отзывы о нас' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Отзывы о нас' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'image', 'crb_banner_image', __( 'Image' ) )
				->set_value_type( 'url' ),
		] )
		->add_tab( __( 'Content section' ), [
			Field::make( 'complex', 'crb_feedbacks', __( 'Feedbacks' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Photo' ) )
						->set_value_type( 'url' ),
					Field::make( 'text', 'name', __( 'Name' ) ),
					Field::make( 'text', 'position', __( 'Position' ) ),
					Field::make( 'text', 'comment', __( 'Comment' ) ),
					Field::make( 'date', 'date', __( 'Date' ) )
						->set_storage_format( 'Y-m-d' ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Portfolio Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-portfolio-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Портфолио' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Портфолио' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'image', 'crb_banner_image', __( 'Image' ) )
				->set_value_type( 'url' ),
		] )
		->add_tab( __( 'Content section' ), [
			Field::make( 'complex', 'crb_gallery', __( 'Gallery' ) )
				->add_fields( [
					Field::make( 'text', 'cat', __( 'Category Name' ) ),
					Field::make( 'complex', 'crb_photos', __( 'Photos' ) )
						->add_fields( [
							Field::make( 'image', 'photo', __( 'Photo' ) ),
							Field::make( 'text', 'text', __( 'Text' ) ),

						] ),
				] ),
		] );

	Container::make( 'post_meta', __( 'Contact Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-contacts-page.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Контакты' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Контакты' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'image', 'crb_banner_image', __( 'Image' ) )
				->set_value_type( 'url' ),
		] )
		->add_tab( __( 'Content section' ), [
			Field::make( 'text', 'crb_tel', __( 'Telephone' ) )
				->set_default_value( '+380 93 216 29 14' ),
			Field::make( 'textarea', 'crb_time', __( 'Time' ) )
				->set_default_value( '<p>Звоните нам с 10:00 до 19:00 с понедельника</p>
                    <p>по пятницу. И с 12:00 до 17:00 по выходным</p>' ),
			Field::make( 'text', 'crb_mail', __( 'Mail' ) )
				->set_default_value( 'korpusmebeli@gmail.com' ),
			Field::make( 'text', 'crb_adr', __( 'Adr' ) )
				->set_default_value( 'Харьков, ул. Динамическая 2а' ),
			Field::make( 'text', 'crb_tg', __( 'Telegram' ) )
				->set_default_value( '#' )
				->set_width( 33 ),
			Field::make( 'text', 'crb_viber', __( 'Viber' ) )
				->set_default_value( '#' )
				->set_width( 33 ),
			Field::make( 'text', 'crb_ms', __( 'Messenger' ) )
				->set_default_value( '#' )
				->set_width( 33 ),
		] )
		->add_tab( __( 'FAQ section' ), [
			Field::make( 'rich_text', 'crb_faq_text', __( 'FAQ Text' ) )
				->set_default_value( '<h3>Обсудим Ваш проект</h3>
                <h4>Поможем с выбором материала и предложим варианты дизайнерских решений будущей мебели прямо на месте</h4>' ),
			Field::make( 'complex', 'crb_faqs', __( 'FAQ' ) )
				->add_fields( [
					Field::make( 'text', 'title', __( 'Title' ) ),
					Field::make( 'textarea', 'text', __( 'Text' ) ),

				] ),
		] );
}

add_action( 'carbon_fields_register_fields', 'crb_attach_services_options' );
function crb_attach_services_options() {
	Container::make( 'post_meta', __( 'Services Settings' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-services.php' )
		->add_tab( __( 'Banner section' ), [
			Field::make( 'separator', 'crb_banner_separator_text', __( 'Text part' ) ),
			Field::make( 'text', 'crb_banner_title', __( 'Title H1' ) )
				->set_default_value( 'Услгуги' ),
			Field::make( 'separator', 'crb_banner_separator_crumbs', __( 'Breadcrumbs part' ) ),
			Field::make( 'text', 'crb_crumbs_1', __( 'Text home' ) )
				->set_default_value( 'Главная' )
				->set_width( 50 ),
			Field::make( 'text', 'crb_crumbs_2', __( 'Txt current' ) )
				->set_default_value( 'Услгуги' )
				->set_width( 50 ),
			Field::make( 'separator', 'crb_banner_separator_media', __( 'Media part' ) ),
			Field::make( 'complex', 'crb_contents', __( 'Content' ) )
				->add_fields( 'cat', [
					Field::make( 'text', 'category_name', __( 'Category name' ) ),
					Field::make( 'image', 'photo', __( 'Banner Photo' ) ),
					Field::make( 'rich_text', 'text', __( 'Content Text' ) ),
					Field::make( 'image', 'photo_2', __( 'Content Photo' ) ),
				] ),
		] )
		->add_tab( __( 'Slider section' ), [
			Field::make( 'rich_text', 'crb_slider_text', __( 'Slider Text' ) )
				->set_default_value( '<h3>Наша фурнитура</h3>
                <p>Конечный потребитель выбирает кухонный комплект, в основном ориентируясь на его внешний вид. А вот мастер-мебельщик знает, что цвет и материал изготовления фасадов, форма дверок и количество полок – это далеко не все характеристики гарнитура. Не менее важной деталью является фурнитура, от надёжности которой зависит долговечность и полноценность функционирования шкафов и тумбочек.</p>' ),
			Field::make( 'complex', 'crb_slider', __( 'Content' ) )
				->add_fields( [
					Field::make( 'image', 'photo', __( 'Content Photo' ) ),
				] ),
		] );
}
