const burgerBtn = document.getElementById( 'burger-menu-btn' )
const burgerMenu = document.getElementsByClassName( 'nav-wrapper' )[ 0 ]
const menuText = document.getElementById( 'menu-desc-toggler' )
const langRu = document.getElementById( 'lang_RU' )
const langUa = document.getElementById( 'lang_UA' )

jQuery( document ).ready( function() {
	//burger-menu handler
	burgerMenu.style.top = `-${burgerMenu.clientHeight}px`

	function burgerHandler() {
		this.classList.toggle( 'burger-active' )
		burgerMenu.classList.toggle( 'nav-active' )

		const trigger = burgerMenu.classList.contains( 'nav-active' )

		function setMenuText() {
			menuText.innerText = trigger ? 'Закрыть' : 'Меню'
			menuText.classList.remove( 'menu-desc-active' )
		}

		menuText.classList.add( 'menu-desc-active' )
		setTimeout( setMenuText, 300 )

		if ( trigger ) {
			burgerMenu.style.top = '100%'
		} else {
			burgerMenu.style.top = `-${burgerMenu.clientHeight}px`
		}
	}

	burgerBtn.addEventListener( 'click', burgerHandler )


	//lang switcher
	function langSwitch( event ) {
		if ( event.target.id === 'lang_RU' ) {
			langRu.classList.add( 'lang-active' )
			langUa.classList.remove( 'lang-active' )
		} else {
			langUa.classList.add( 'lang-active' )
			langRu.classList.remove( 'lang-active' )
		}
	}

	// langRu.addEventListener('click', langSwitch)
	// langUa.addEventListener('click', langSwitch)


	//main slider
	//tabs
	jQuery( '.main-slider-wrapper .category' ).click( function() {
		jQuery( '.main-slider-wrapper .category' )
			.removeClass( 'active' )
			.eq( jQuery( this ).index() )
			.addClass( 'active' );
		jQuery( '.photo-category' )
			.hide()
			.eq( jQuery( this ).index() )
			.fadeIn();
		jQuery( '.tabs-container .content-tab .tab-content-wrapper' )
			.hide()
			.eq( jQuery( this ).index() )
			.fadeIn();
		jQuery( '.home-page .categories-section .category .progress' )
			.removeClass( 'active' )
			.eq( jQuery( this ).index() )
			.addClass( 'active' );
	} ).eq( 0 ).addClass( 'active' );

	jQuery( '.main-slider-wrapper .category .progress' )
		.eq( 0 ).addClass( 'active' );


	//slider (home page)
	function handleProgress( slick, currentSlide, i ) {
		const slideFactor = 100 / slick.slideCount
		const crntSlide = currentSlide + 1
		const currentWidth = slideFactor * crntSlide
		jQuery( `.main-slider-wrapper .category-${i + 1} .progress` ).css( 'width', `${currentWidth}%` )
	}

	const mainscreenSlideOptions = {
		arrows: false,
		autoplay: true,
		dots: false,
		fade: true,
	}

	jQuery( '.mainscreen-tab' ).each( function( i ) {
		jQuery( `.mainscreen-tab-${i + 1}` ).slick( mainscreenSlideOptions ).on( 'beforeChange', ( _, slick, currentSlide, __ ) => handleProgress( slick, currentSlide, i ) )
	} )


	//galery
	jQuery( '.slider-gallery-wrapper' ).slick( {
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: true,
		maxWidth: 400,
		prevArrow: '.our-works-btn-prev',
		nextArrow: '.our-works-btn-next',
		responsive: [
			{
				breakpoint: 1270,
				settings: {
					slidesToShow: 3,
					centerMode: false,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 425,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: false,
					variableWidth: true
				}
			}
		]
	} )


	//steps tabs
	var elemCounter = jQuery( '#steps-section .tabs-progress .item' ).length

	jQuery( '#steps-section .tabs-progress .item ' ).click( function() {
		var percVar = 100 / elemCounter
		var currentElem = jQuery( this ).index() + 1

		jQuery( '.tabs-progress .item' )
			.removeClass( 'active' )
			.eq( jQuery( this ).index() )
			.addClass( 'active' );
		jQuery( '.right-section .photo-section' )
			.hide()
			.eq( jQuery( this ).index() )
			.fadeIn();
		jQuery( '#progress-steps-section .counter .current' )
			.text( jQuery( this ).index() + 1 );
		jQuery( '#progress-line-steps-section .line' )
			.css( 'width', percVar * currentElem + '%' )
	} ).eq( 0 ).addClass( 'active' );

	jQuery( '#progress-steps-section .counter .all ' )
		.text( jQuery( '#steps-section .tabs-progress .item' ).length );

	jQuery( '#progress-line-steps-section .line' )
		.css( 'width', 100 / elemCounter + '%' );


	//partners-slider
	jQuery( '.our-partners-slider ' )
		.slick( {
			slidesToScroll: 1,
			slidesToShow: 3,
			centerMode: true,

			prevArrow: '.our-partners-btn-prev',
			nextArrow: '.our-partners-btn-next',

			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 425,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						centerMode: false
					}
				}
			]
		} );

	jQuery( '.feedback-section .slider-wrapper' ).slick( {
		slidesToShow: 3,
		centerMode: true,
		prevArrow: '.our-feedbacks-btn-prev',
		nextArrow: '.our-feedbacks-btn-next',
		responsive: [
			{
				breakpoint: 1152,
				settings: {
					centerMode: false
				}
			},
			{
				breakpoint: 920,
				settings: {
					slidesToShow: 2,
					centerMode: false
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					centerMode: true
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					centerMode: false
				}
			}
		]
	} )

	jQuery( '.portfolio-page .portfolio-cases .tabs li' ).click( function() {
		jQuery( '.portfolio-cases .tabs li' )
			.removeClass( 'active' )
			.eq( jQuery( this ).index() )
			.addClass( 'active' );
		jQuery( '.portfolio-page .content-tab' )
			.hide()
			.eq( jQuery( this ).index() )
			.fadeIn();
	} ).eq( 0 ).addClass( 'active' );
	jQuery( '.portfolio-page .content-tab' )
		.eq( 0 ).addClass( 'active' );


	//accordion
	const accordionTab = jQuery( '.contact-form-section .questions-wrapper .drop-container .drop-item' )
	const accordionWrapper = jQuery( '.contact-form-section .questions-wrapper .drop-container .drop-item p' )
	accordionTab.click( function() {
		accordionTab
			.eq( jQuery( this ).index() )
			.toggleClass( 'active-accord' );
		accordionWrapper
			.eq( jQuery( this ).index() )
			.slideToggle()
	} )


	//inputmask
	const telInp = jQuery( 'input[type=tel]' )
	telInp.inputmask( { 'mask': '+38(099) 999-99-99' } )
} )

