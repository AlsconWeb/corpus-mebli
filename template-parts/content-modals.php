 <div class="contact-modal modal tnx-modal" id="tnx-modal">
    <a href="#close-modal" rel="modal:close" class="cross"></a>
    <div class="content">
        <h2>Спасибо!</h2>
        <h4>Ваши данные успешно отправлены. Ждите звонк от нашего менеджера в рабочее время.</h4>
    </div>
</div>

 <div id="form-modal" class="form-modal modal tnx-modal" >
     <a href="#close-modal" rel="modal:close" class="cross"></a>
     <div class="content">
         <h2>Вам нужна консультация?</h2>
         <h4>Заполните простую форму и получите консультацию специалиста по Вашему вопросу</h4>
         <?php echo do_shortcode('[contact-form-7 id="29" title="Contact form 1" html_class="contact-form"]') ?>
         <div class="contacts-info">
             <p>Не хотите ждать? Наберите нас сами!</p>
             <p>
                 <a href="tel:<?php echo carbon_get_theme_option( 'phone' ); ?>"><?php echo carbon_get_theme_option( 'phone' ); ?>l</a>
             </p>
             <p>Мы работаем: Пн-Пт с 9:00 до 19:00; Сб-Вс 11:00-15:00</p>
         </div>
     </div>
 </div>