<div class="slider-wrapper">
    <div class="container">
        <div class="wrapper slider-gallery-wrapper">
            <?php
            $slider = carbon_get_the_post_meta( 'crb_slider' );

            foreach ($slider as $item){ ?>
                <div class="product-card item">
	                <?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                </div>
            <?php } ?>

        </div>
    </div>
    <div class="btn-container">
        <button class="slider-btn btn-l our-works-btn-prev"></button>
        <button class="slider-btn btn-r our-works-btn-next"></button>
    </div>
</div>
