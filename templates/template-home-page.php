<?php
/*
Template Name: Home Page
*/
get_header();
?>

    <main class="home-page page" id="home-page">
        <section class="first-screen main-page tabs-head-page" id="first-screen">
            <div class="container main-slider-wrapper">
                <div class="image-wrapper" id="main-image-slider">
					<?php
					$cat = carbon_get_the_post_meta( 'crb_categories' );
					$i   = 1;
					foreach ( $cat as $item ) {
						?>
                        <div class="photo-category ">
                            <div class="mainscreen-tab mainscreen-tab-<?php echo $i; ?>">
								<?php foreach ( $item['photos'] as $slide ) { ?>
                                    <div class="tab-slide">
	                                    <?php echo wp_get_attachment_image( $slide['photo'], 'full' ); ?>
                                    </div>
								<?php } ?>
                            </div>
                        </div>
						<?php $i ++;
					} ?>
                </div>
                <div class="text-section main-page-head-text">
                    <h1><?php echo carbon_get_the_post_meta( 'crb_banner_title' ); ?></h1>
                    <p class="h4"><?php echo carbon_get_the_post_meta( 'crb_banner_sub_title' ); ?></p>
                    <a href="#form-modal" rel="modal:open"
                       class="btn"><?php echo carbon_get_the_post_meta( 'crb_banner_btn' ); ?></a>
                </div>
                <div class="categories-section">
					<?php
					$cat = carbon_get_the_post_meta( 'crb_categories' );
					$i   = 0;
					foreach ( $cat as $item ) {
						?>
                        <div class="category category-<?php echo $i + 1; ?>">
                            <div class="progress"></div>
                            <span><?php echo $item['cat'] ?></span>
                        </div>
						<?php $i ++;
					} ?>

                </div>
            </div>
        </section>

        <section class="second-screen light-section" id="second-screen">
            <div class="container">
                <div class="product-card item main-item">
                    <h2 class="h1"><?php echo carbon_get_the_post_meta( 'crb_services_title' ); ?></h2>
                    <p class="h4"><?php echo carbon_get_the_post_meta( 'crb_services_sub_title' ); ?></p>
                    <a href="<?php echo carbon_get_the_post_meta( 'crb_services_link' ); ?>"
                       class="btn"><?php echo carbon_get_the_post_meta( 'crb_services_btn' ); ?></a>
                </div>

				<?php
				$services = carbon_get_the_post_meta( 'crb_services' );

				foreach ( $services as $item ) { ?>
                    <div class="product-card item">
						<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                        <a href="<?php echo $item['btn_link']; ?>"class="about"><?php echo $item['btn']; ?></a>
                    </div>
				<?php } ?>

            </div>
        </section>

        <section class="third-screen advantages-section dark-section" id="third-screen">
            <div class="container">
                <h3><?php echo carbon_get_the_post_meta( 'crb_benefits_title' ); ?></h3>
                <div class="wrapper advantages">
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_1' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_2' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_3' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_4' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_5' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_6' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_7' ); ?></p>
                    </div>
                    <div class="item">
                        <p><?php echo carbon_get_the_post_meta( 'crb_benefits_8' ); ?></p>
                    </div>
                </div>
            </div>
        </section>

        <!--  GLOBAL CONTENT  -->
        <section class="fourth-screen our-works-section slider-gallery-1 light-section" id="our-works-section">
            <h3><?php echo carbon_get_the_post_meta( 'crb_slider_title' ); ?></h3>
            <div class="slider-wrapper">
                <div class="container">
                    <div class="wrapper slider-gallery-wrapper">
						<?php
						$slider = carbon_get_the_post_meta( 'crb_slider' );

						foreach ( $slider as $item ) { ?>
                            <div class="product-card item">
								<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                                <a href="<?php echo $item['btn_link']; ?>" class="about"><?php echo $item['btn']; ?></a>
                            </div>
						<?php } ?>
                    </div>
                </div>
                <div class="btn-container">
                    <button class="slider-btn btn-l our-works-btn-prev"></button>
                    <button class="slider-btn btn-r our-works-btn-next"></button>
                </div>
            </div>
        </section>

        <section class="fifth-screen steps-section dark-section" id="steps-section">
            <div class="container">
                <div class="left-section">
                    <div class="description">
						<?php echo apply_filters( 'the_content', carbon_get_the_post_meta( 'crb_steps_content' ) ); ?>

                        <div class="progress" id="progress-steps-section">
                            <div class="counter">
                                <span class="current">1</span> | <span class="all"></span>
                            </div>
                            <div class="progress-line" id="progress-line-steps-section">
                                <div class="line"></div>
                            </div>
                        </div>
                        <div class="tabs-progress">
							<?php
							$slider = carbon_get_the_post_meta( 'crb_steps_slider' );

							foreach ( $slider as $item ) { ?>
                                <div class="item">
									<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                                </div>
							<?php } ?>

                        </div>
                    </div>
                </div>
                <div class="right-section">
					<?php
					foreach ( $slider as $item ) { ?>
                        <div class="photo-section" id="steps-photo-section">
							<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                        </div>
					<?php } ?>

                </div>
            </div>
        </section>

        <!--  GLOBAL CONTENT PARTNERS_SLIDER -->
        <section class="sixth-screen our-partners-section light-section" id="our-partners-section">
			<?php get_template_part( 'template-parts/content', 'partners-slider' ); ?>
        </section>

        <!--  GLOBAL CONTENT CONTACT_FORM-->
        <section class="seventh-screen contact-form-section dark-section" id="contact-form">
			<?php get_template_part( 'template-parts/content', 'contact-form' ); ?>
        </section>

        <!--  GLOBAL CONTENT INSTARGAM_POSTS_SECTION-->
        <section class="eight-screen insagram-section light-section" id="instagram-section">
			<?php get_template_part( 'template-parts/content', 'instagram-section' ); ?>
        </section>
    </main>

<?php
get_footer();
