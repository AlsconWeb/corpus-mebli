<?php
/*
Template Name: Portfolio Page
*/
?>

<?php get_header(); ?>
    <main class="portfolio-page page" id="portfolio-page">
        <section class="first-screen" id="first-screen">
            <div class="container"
                 style="background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo carbon_get_the_post_meta( 'crb_banner_image' ); ?>')">
                <div class="text-section without-tabs inner-page">
                    <h1><?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?></h1>
                </div>
                <div class="breadcrumbs">
                    <a href="<?php echo get_home_url(); ?>">
						<?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?>
                    </a> / <span><?php echo carbon_get_the_post_meta( 'crb_crumbs_2' ); ?></span>
                </div>
            </div>
        </section>

        <section class="second-screen portfolio-cases light-section" id="portfolio-cases-section">
            <div class="container">
                <div class="tabs random-navigation-section">
                    <ul>
                        <li>Все работы</li>
						<?php
						$gallery = carbon_get_the_post_meta( 'crb_gallery' );

						foreach ( $gallery as $item ) {
							?>
                            <li><?php echo $item['cat'] ?></li>
						<?php } ?>
                    </ul>
                </div>
                <div class="wrapper">
                    <div class="content-tab">
                        <div class="helper">
							<?php
							foreach ( $gallery as $item ) {
								foreach ( $item['crb_photos'] as $photo ) { ?>
                                    <div class="product-card item">
										<?php echo wp_get_attachment_image( $photo['photo'], 'full' ); ?>
                                        <div class="about"><?php echo $photo['text'] ?></div>
                                    </div>
								<?php }
							}
							?>
                        </div>
                    </div>
					<?php
					foreach ( $gallery as $item ) {
						?>
                        <div class="content-tab">
                            <div class="helper">
								<?php foreach ( $item['crb_photos'] as $photo ) { ?>
                                    <div class="product-card item">
										<?php echo wp_get_attachment_image( $photo['photo'], 'full' ); ?>
                                        <div class="about"><?php echo $photo['text'] ?></div>
                                    </div>
								<?php } ?>
                            </div>
                        </div>
					<?php } ?>

                </div>
            </div>
        </section>
    </main>

<?php get_footer();
