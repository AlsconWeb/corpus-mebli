<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package korpus-mebli
 */

?>

	<footer id="footer" class="footer">
        <div class="container">
            <div class="random-navigation-section">
                <ul>
                    <li><a href="/">Жилая площадь</a></li>
                    <li><a href="/">Офисы</a></li>
                    <li><a href="/">Мебель для торговых площадей</a></li>
                    <li><a href="/">Детская</a></li>
                    <li><a href="/">Кухня</a></li>
                    <li><a href="/">Санузел</a></li>
                </ul>
            </div>
            <div class="middle-section">
                <div class="left section">
                    <ul>
                        <li><a href="#">Портфолио</a></li>
                        <li><a href="#">Отзывы</a></li>
                        <li><a href="#">Партнёры</a></li>
                        <li><a href="#">Контакты</a></li>
                        <li><a href="#">Вопрос - ответ</a></li>
                    </ul>
                </div>
                <div class="mid section">
                    <a href="/">
                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="KORPUS MEBLI"/>
                    </a>
                    <p>Мы производим мебель для b2b и b2с сегментов. Воплощаем на рынке Украины уникальные проекты совместно с нашими партнерами. Мы подробно вникаем в каждый проект, чтобы каждое новое изделие создавать уникальным и совершенным.</p>
                </div>
                <div class="right section">
                    <ul>
                        <li><a href="tel:<?php echo carbon_get_theme_option('phone'); ?>"><?php echo carbon_get_theme_option('phone'); ?></a></li>
                        <li><a href="mailto:<?php echo carbon_get_theme_option('email'); ?>"><?php echo carbon_get_theme_option('email'); ?></a></li>
                        <li><a target="_blank" href="<?php echo carbon_get_theme_option('adr_link'); ?>"><?php echo carbon_get_theme_option('adr'); ?></a></li>
                        <li><?php echo carbon_get_theme_option('time'); ?></li>
                        <li>Instagram: <a target="_blank" href="<?php echo carbon_get_theme_option('inst_link'); ?>"> <?php echo carbon_get_theme_option('text_link'); ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="bottom-section">
	            <?php echo carbon_get_theme_option('additional'); ?>
            </div>
        </div>
    </footer><!-- #footer -->

<?php wp_footer(); ?>
</body>
</html>
