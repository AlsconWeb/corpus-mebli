<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package korpus-mebli
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/futura/stylesheet.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header id="mainhead" class="header">
    <div class="container head-content">
        <a class="btn btn-border" href="tel:<?php echo carbon_get_theme_option( 'phone' ); ?>"><?php echo carbon_get_theme_option( 'phone' ); ?></a>
        <a href="/">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="KORPUS MEBLI"/>
        </a>

<!--        <div class="language-switch">-->
<!--            <a class="lang" id="lang_UA" href="#">Ua</a>-->
<!--            <a class="lang lang-active" id="lang_RU" href="#">Ru</a>-->
<!--        </div>-->
        <div class="menu" id="burger-menu-btn">
            <div class="burger"></div>
            <span class="menu-desc" id="menu-desc-toggler">Меню</span>
            <div class="nav-wrapper">
<!--                <div class="language-switch">-->
<!--                    <a class="lang" id="lang_UA" href="#">Ua</a>-->
<!--                    <a class="lang lang-active" id="lang_RU" href="#">Ru</a>-->
<!--                </div>-->
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'nav-menu',
                        'container' => 'nav',
                        'container_class' => 'nav-container',
                        'menu_class' => 'links'
                    )
                );
                ?>
                <a class="btn btn-border" href="tel:<?php echo carbon_get_theme_option( 'phone' ); ?>"><?php echo carbon_get_theme_option( 'phone' ); ?></a>
            </div>
        </div>
    </div>
</header> <!-- header -->

<?php get_template_part('template-parts/content', 'modals'); ?>
