<?php

/*
Template Name: Project Page
*/

get_header(); ?>

    <main class="project-page" id="project-page">
        <section class="first-screen" id="first-screen">
            <div class="container"
                 style="height: 100%; position: relative; background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo carbon_get_the_post_meta( 'crb_banner_image' ); ?>')">
                <div class="text-section without-tabs inner-page">
                    <h1><?php echo carbon_get_the_post_meta( 'crb_banner_title' ); ?></h1>
                </div>
                <div class="breadcrumbs">
                    <a href="<?php echo get_home_url(); ?>">
						<?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?>
                    </a> / <a
                            href="<?php echo carbon_get_the_post_meta( 'crb_crumbs_3' ); ?>"><?php echo carbon_get_the_post_meta( 'crb_crumbs_2' ); ?></a>
                    / <span><?php echo carbon_get_the_post_meta( 'crb_crumbs_4' ); ?></span>
                </div>
            </div>
        </section>

        <section class="second-screen project-section light-section">
            <div class="container">
                <div class="content-tab">
					<?php
					$content = carbon_get_the_post_meta( 'crb_content' );


					foreach ( $content as $item ) {
						?>
                        <div class="tab-content-wrapper">
                            <div class="image">
								<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                            </div>
                            <div class="desc">
                            <?php echo apply_filters( 'the_content', $item['text'] ); ?>
                            </div>
                        </div>
					<?php } ?>
                </div>
            </div>
        </section>

        <!-- FURNITURE_SLIDER -->
        <section class="third-screen tabs-content light-section">
            <div class="container title-wrapper">
	            <?php echo apply_filters( 'the_content', carbon_get_the_post_meta( 'crb_slider_text' ) ); ?>
            </div>
		    <?php get_template_part('template-parts/content', 'slider-gallery1'); ?>
        </section>

        <!--  GLOBAL CONTENT CONTACT_FORM-->
        <section class="contact-form-section dark-section" id="contact-form">
			<?php get_template_part( 'template-parts/content', 'contact-form' ); ?>
        </section>

        <!--  GLOBAL CONTENT INSTARGAM_POSTS_SECTION-->
        <section class="insagram-section light-section" id="instagram-section">
			<?php get_template_part( 'template-parts/content', 'instagram-section' ); ?>
        </section>

    </main>

<?php get_footer();