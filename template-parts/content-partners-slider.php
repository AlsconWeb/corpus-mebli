<div class="container">
    <h3><?php echo carbon_get_the_post_meta( 'crb_partners_title' ); ?></h3>
    <div class="slider-wrapper">
        <div class="wrapper our-partners-slider">
            <?php
            $slider = carbon_get_the_post_meta( 'crb_partners_slider' );

            foreach ($slider as $item){ ?>
                <div class="slide" style="background-image: url(<?php echo $item['photo'] ?>)"></div>

            <?php } ?>
        </div>
        <div class="btn-container">
            <button class="slider-btn btn-l our-partners-btn-prev"></button>
            <a href="<?php echo carbon_get_the_post_meta( 'btn_steps_link' ); ?>" class="btn">Узнать подробнее</a>
            <button class="slider-btn btn-r our-partners-btn-next"></button>
        </div>
    </div>
</div>
