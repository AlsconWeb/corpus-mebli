<div class="container">
    <h4>
        Будьте в курсе всех новостей и выгодных предложений от KORPUS MEBELI. Подписывайтесь на наш Instagram
        <a href="<?php echo carbon_get_theme_option('inst_link') ?>"><?php echo carbon_get_theme_option('text_link') ?></a>
    </h4>
    <div class="instagram-photos">
        <?php
        $ins = carbon_get_theme_option('crb_instagram');

        foreach ($ins as $item){ ?>
            <div class="wrapper">
                <a href="<?php echo $item['link'] ?>">
	                <?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                </a>
            </div>
        <?php } ?>

    </div>
</div>
