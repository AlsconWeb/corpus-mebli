<?php
/*
Template Name: Feedbacks Page
*/

get_header();

?>

    <main class="page feedbacks-page" id="feetbacks-page">
        <section class="first-screen" id="first-screen">
            <div class="container"
                 style="background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo carbon_get_the_post_meta( 'crb_banner_image' ); ?>')">
                <div class="text-section without-tabs inner-page">
                    <h1><?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?></h1>
                </div>
                <div class="breadcrumbs">
                    <a href="<?php echo get_home_url(); ?>">
						<?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?>
                    </a> / <span><?php echo carbon_get_the_post_meta( 'crb_crumbs_2' ); ?></span>
                </div>
            </div>
        </section>

        <section class="second-screen feedback-section light-section" id="feedback-section">
            <div class="container">
                <div class="slider-wrapper">
					<?php
					$feedbacks = carbon_get_the_post_meta( 'crb_feedbacks' );

					foreach ( $feedbacks as $item ) {?>
                        <div class="slide">
                            <div class="top">
                                <div class="image"
                                     style="background-image: url(<?php echo $item['photo'] ?>)"></div>
                                <div class="title">
                                    <h4><?php echo $item['name'] ?></h4>
                                    <p><?php echo $item['position'] ?></p>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="text">
                                    <p><?php echo $item['comment'] ?></p>
                                </div>
                                <span><?php echo $item['date'] ?></span>
                            </div>
                        </div>
					<?php } ?>
                </div>
                <div class="btn-container">
                    <button class="slider-btn btn-l our-feedbacks-btn-prev"></button>
                    <button class="slider-btn btn-r our-feedbacks-btn-next"></button>
                </div>
            </div>
        </section>

        <!--  GLOBAL CONTENT CONTACT_FORM-->
        <section class="seventh-screen contact-form-section dark-section" id="contact-form">
			<?php get_template_part( 'template-parts/content', 'contact-form' ); ?>
        </section>

        <!--  GLOBAL CONTENT INSTARGAM_POSTS_SECTION-->
        <section class="eight-screen insagram-section light-section" id="instagram-section">
			<?php get_template_part( 'template-parts/content', 'instagram-section' ); ?>
        </section>

    </main>

<?php get_footer();
