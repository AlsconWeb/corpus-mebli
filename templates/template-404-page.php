<?php

/*
Template Name: 404 Page
*/

get_header(); ?>

<main class="wrong-section" id="wrong-section">
    <div class="first-screen">
        <div class="container">
            <h1>Похоже, что-то пошло не так :(</h1>
            <h2>Это небольшие технические неполадки и мы уже работаем над их исправлением.</h2>
            <a class="btn" href="/">Вернуться на главную</a>
        </div>
    </div>
</main>

<?php
get_footer();