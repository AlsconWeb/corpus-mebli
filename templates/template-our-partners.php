<?php

/*
Template Name: Partners Page
*/

get_header(); ?>

    <main class="our-partners" id="our-partners">
        <section class="first-screen" id="first-screen">
            <div class="container"
                 style="background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('<?php echo carbon_get_the_post_meta( 'crb_banner_image' ); ?>')">
                <div class="text-section without-tabs inner-page">
                    <h1><?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?></h1>
                </div>
                <div class="breadcrumbs">
                    <a href="<?php echo get_home_url(); ?>">
						<?php echo carbon_get_the_post_meta( 'crb_crumbs_1' ); ?>
                    </a> / <span><?php echo carbon_get_the_post_meta( 'crb_crumbs_2' ); ?></span>
                </div>
            </div>
        </section>

        <section class="second-screen about-partners-section light-section">
            <div class="container">
                <div class="content-tab">
                    <div class="tab-content-wrapper">
                        <div class="image">
							<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'crb_about_photo' ), 'full' ); ?>
                        </div>
                        <div class="desc">
							<?php echo apply_filters( 'the_content', carbon_get_the_post_meta( 'crb_about_text' ) ); ?>
                        </div>
                    </div>
                </div>
                <div class="logo-section">
					<?php
					$partners = carbon_get_the_post_meta( 'crb_partners' );

					foreach ( $partners as $item ) { ?>
                        <div class="item">
	                        <?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>
                        </div>
					<?php } ?>
                </div>
            </div>
        </section>

        <!--  GLOBAL CONTENT CONTACT_FORM-->
        <section class="contact-form-section dark-section" id="contact-form">
			<?php get_template_part( 'template-parts/content', 'contact-form' ); ?>
        </section>

        <!--  GLOBAL CONTENT INSTARGAM_POSTS_SECTION-->
        <section class="insagram-section light-section" id="instagram-section">
			<?php get_template_part( 'template-parts/content', 'instagram-section' ); ?>
        </section>

    </main>

<?php get_footer();
